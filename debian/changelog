emacs-buttercup (1.37-1) unstable; urgency=medium

  * New upstream release
  * Add Salsa CI configuration file
  * Add autopkgtest to run upstream `make check'

 -- Xiyue Deng <manphiz@gmail.com>  Mon, 17 Feb 2025 22:53:48 -0800

emacs-buttercup (1.36-1) unstable; urgency=medium

  * Team upload.

  [ Xiyue Deng ]
  * New upstream release
  * Update d/watch with filenamemangle for generating sane package name
  * Update Standards-Version to 4.7.0; no change needed

  [ Jeremy Sowden ]
  * d/.gitignore: ignore build artefacts
  * d/rules: remove unnecessary dh_compress override
  * d/rules: enable `DH_VERBOSE` unless "terse" build-option is in use
  * d/rules: dh_auto_build just runs `make check`, so suppress it
  * d/elpa-test, d/rules: remove dh_auto_test override since it doesn't
    run the test-suite.  Remove the override from d/rules, disable
    dh_elpa_test in d/elpa-test, and leave dh_auto_test to run `make
    check`.

 -- Jeremy Sowden <azazel@debian.org>  Wed, 11 Sep 2024 18:36:24 +0100

emacs-buttercup (1.35-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:13:08 +0900

emacs-buttercup (1.35-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 11:40:28 +0900

emacs-buttercup (1.35-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * New upstream release
  * Add myself to uploaders

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 14 Apr 2024 11:59:29 +0800

emacs-buttercup (1.34-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Team upload
  * New upstream release
  * Migrate to debhelper-compat version 13
  * Add Upstream-Contact in d/copyright
  * Add d/upstream/metadata

  [ Jeremy Sowden ]
  * d/control: set `Rules-Requires-Root: no`
  * d/s/lintian-overrides: update signature check override

 -- Jeremy Sowden <azazel@debian.org>  Tue, 19 Mar 2024 21:14:49 +0000

emacs-buttercup (1.31-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Team upload.
  * New upstream release.
    - Fixes compatibility with Emacs 29.1.  Closes: #1050459.
  * Refresh d/p/debian-changes.
  * Drop "Built-Using" from d/control as it should not be used with
    arch:all package as suggested by lintian.
  * Update d/watch to use version 4 features.
  * Update Standards-Version to 4.6.2.  No change needed.
  * Filter "nocheck" in DEB_BUILD_OPTIONS as per lintian warning.

  [ Sean Whitton ]
  * Remove myself from Uploaders.

 -- David Bremner <bremner@debian.org>  Tue, 29 Aug 2023 18:28:07 -0300

emacs-buttercup (1.26-4) unstable; urgency=medium

  * Remove native compilation workarounds; rely on emacs doing the right
    thing. This requires a versioned dependence on emacs.

 -- David Bremner <bremner@debian.org>  Sat, 14 Jan 2023 13:40:38 -0400

emacs-buttercup (1.26-3) unstable; urgency=medium

  * fix second typo in bin/buttercup introduced in 1.26-1
  * use native-comp-eln-load-path to redirect output
  * Add myself to uploaders

 -- David Bremner <bremner@debian.org>  Sun, 09 Oct 2022 06:54:11 -0300

emacs-buttercup (1.26-2) unstable; urgency=medium

  * Team upload
  * Fix typo in buttercup script introduced in 1.26-1

 -- David Bremner <bremner@debian.org>  Sat, 24 Sep 2022 14:03:40 -0300

emacs-buttercup (1.26-1) unstable; urgency=medium

  * Team upload

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

  [ David Bremner ]
  * New upstream version
  * Set native compilation output to temporary directory (Closes: #1020095).

 -- David Bremner <bremner@debian.org>  Sat, 24 Sep 2022 08:03:57 -0300

emacs-buttercup (1.24-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 17 Jan 2021 11:24:30 -0700

emacs-buttercup (1.22-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 15 Jul 2020 15:27:18 -0700

emacs-buttercup (1.21-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 14 Mar 2020 14:04:48 -0700

emacs-buttercup (1.20-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 29 Feb 2020 08:53:37 -0700

emacs-buttercup (1.19-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 15 Jan 2020 06:46:10 -0700

emacs-buttercup (1.18-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 10 Oct 2019 10:24:43 -0700

emacs-buttercup (1.16-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 08:50:20 -0300

emacs-buttercup (1.16-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 07 Dec 2018 13:29:14 -0700

emacs-buttercup (1.15-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 30 Nov 2018 09:54:42 -0700

emacs-buttercup (1.14-1) unstable; urgency=medium

  * New upstream release.
  * Drop trailing slash from Vcs-Git.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 20 Oct 2018 12:48:27 -0700

emacs-buttercup (1.13-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 08 Sep 2018 10:30:45 -0700

emacs-buttercup (1.12-1) unstable; urgency=medium

  * New upstream release.
  * Switch to dgit-maint-merge(7) workflow
    - Add d/source/options
    - Add d/source/patch-header.
  * Drop --parallel from d/rules.
  * Points Vcs-* at salsa.
  * Update Maintainer field for team rename.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 21 Jul 2018 17:29:04 +0800

emacs-buttercup (1.11-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 12 Mar 2018 18:17:18 -0700

emacs-buttercup (1.9-1) unstable; urgency=medium

  * New upstream version.
  * Update buttercup(1) with new options.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 27 Sep 2017 16:33:10 -0700

emacs-buttercup (1.8-1) unstable; urgency=medium

  * New upstream release.
  * Bump copyright years.
  * Bump std-ver to 4.1.0 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 02 Sep 2017 13:03:43 -0700

emacs-buttercup (1.7-1) unstable; urgency=medium

  * New upstream release.
    - Update buttercup.1 for several new options.
  * Drop 0002-exclude-hidden-files-and-dirs-in-test-root.patch
    Merged upstream.
  * Bump debhelper compat to 10.
  * Bump standards version to 4.0.0 (no changes required).
  * Drop d/source/options for dgit.
    Although dgit does not require dropping this file, it serves no
    purpose and gets in the way of uploading.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 19 Jun 2017 12:08:00 +0100

emacs-buttercup (1.5-2) unstable; urgency=medium

  * Add patch to fix regexp so that /usr/bin/buttercup ignores tests in
    hidden directories in the root of a source tree.
    In particular, this ignores phantom tests found in the .pc/ directory.
  * Bump standards version to 3.9.8 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 13 May 2016 15:55:20 -0700

emacs-buttercup (1.5-1) unstable; urgency=medium

  * New upstream version.
  * Drop patch backporting a fix present in this version.
  * Refresh remaining patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 29 Mar 2016 15:00:30 -0700

emacs-buttercup (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #816835)

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 09 Mar 2016 12:47:30 -0700
